﻿const express = require("express");
const server = express();
const bodyParser = require("body-parser");
const port = 3000;

require("./database");
require("./models/user");
require("./models/animals");

server.use(bodyParser.json());

server.get("/", (req, res) => {
  res.send({ message: "Api" });
});

server.use("/api", require("./controllers/user"));
server.use("/api", require("./controllers/animals"));

server.listen(port, function() {
  console.log(`Server is running on port ${port}!`);
});