﻿const router = require("express").Router();
const mongoose = require("mongoose");
const auth = require("../middleware/auth");

const User = mongoose.model("User");

router.post("/user/register", async (req, res) => {
  const { email } = req.body;

  try {
    if (await User.findOne({ email })) {
      return res.status(400).json({ error: "Email already exists." });
    }

    const user = await User.create(req.body);
    return res.json({ user });
  } catch (error) {
    return res.status(400).json({ error: "Register failed" });
  }
});

router.post("/user/authenticate", async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.findOne({ email });

    if (!user) {
      return res.status(400).json({ error: "User not found." });
    }

    if (!(await user.compareHash(password))) {
      return res.status(400).json({ error: "Invalid password." });
    } 

    return res.json({
      user,
      token: user.generateToken()
    });
  } catch (error) {
    return res.status(400).json({ error: "User authentication failed" });
  }
});

router.use(auth);

router.get("/user/profile", async (req, res) => {
  try {
    const { userId } = req;

    const user = await User.findById(userId);

    return res.json({ user });
  } catch (error) {
    return res.status(400).json({ error: "Can't get user information" });
  }
});

module.exports = router;