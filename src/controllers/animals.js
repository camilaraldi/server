﻿const express = require('express');
const router = express.Router();

const auth = require('../middleware/auth');
const Animals = require('../models/animals');

router.use(auth);

router.get('/animals', async (req, res) => {
  try {
    const animals = await Animals.find().populate(['creator']);

    return res.send({ animals });
  } catch (err) {
    return res.status(400).send({ error: 'Error loading animals.' });
  }
});

router.post('/animals', async (req, res) => {
  try {
    const animal = await Animals.create({ ...req.body, creator: req.userId });

    await animal.save();
    
    return res.send({ animal });
  } catch (err) {
    return res.status(400).send({ error: 'Error creating new animal.' });
  }
});

router.get('/animals/user', async (req, res) => {
  try {
    const animal = await Animals.findOne({ creator: req.userId });

    return res.send({ animal });
  } catch (err) {
    return res.status(400).send({ error: 'Error loading animals.' });
  }
});


module.exports = router;