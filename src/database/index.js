﻿const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/admin', { useNewUrlParser: true });
mongoose.set('useCreateIndex', true);
mongoose.Promise = global.Promise;

module.exports = mongoose;