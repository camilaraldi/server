﻿const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Animals = new Schema({
  breed: {
    type: String
  },
  gender: {
    type: String,
    required: true
  },
  age: {
    type: String
  },
  location: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  image: {
    type: String,
    required: true
  },
  contact: {
    type: String,
    required: true
  },
  creator: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
});

module.exports = mongoose.model('animals', Animals);